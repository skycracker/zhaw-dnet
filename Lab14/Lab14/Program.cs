﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Lab14
{
    class Program
    {

        delegate int Function(int x);
        delegate int Function2(int x, int k1, int k2, int k3);

        static int[] ProcessData(Function f, int[] data)
        {
            int[] result = new int[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                result[i] = f(data[i]);
            }
            return result;
        }

        static int[] ProcessData2(Function2 f, int[] data)
        {
            int[] result = new int[data.Length/4];
            for (int i = 0; i < data.Length/4; i++)
            {
                result[i] = f(data[i],data[i+1],data[i+2],data[i+3]);
            }
            return result;
        }

        static void Main(string[] args)
        {
            var values = new int[] { 1, 2, 3, 4, 5, 6, 2, 1, 8 };

            // Aufgabe 1
            Console.WriteLine(" Aufgbe 1: " + string.Join(",", ProcessData(x => x*x, values)));

            // Aufgabe 2
            Console.WriteLine(" Aufgbe 2: " + string.Join(",", ProcessData2( (x, k1, k2, k3) =>  (k1*x*x + k2*x + k3) , values)));

            // Aufgbae 3
            //Console.WriteLine(" Aufgbe 3: " + string.Join(",", values.Select()));

            // Aufgbae 4
            Console.WriteLine(" Aufgbe 4: " + string.Join(",", from v in values where v > 3 & v < 6 select v));

            Console.ReadKey();
        }
    }
}
