﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab01
{

    enum PrimeType { Prim, NotPrim };

    class Program
    {
        static void Main(string[] args)
        {
            int length = 1;


            Console.WriteLine(" .NET Lab 01 - Eratosthenes");
            Console.WriteLine(" --------------------------");

            Console.WriteLine("\n Please specify the largest number: ");
            Console.Write(" ");
            length = Int32.Parse(Console.ReadLine()) + 1;

            Console.WriteLine("\n");

            PrimeType[] array = Eratosthenes(length);
            for (int i = 1; i < length; i++)
            {
                Console.Write(" " + i.ToString("000"));
                if (i % 10 == 0)
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine("\n");

            // Static array
            int[] prims = returnStatic(array);
            Console.Write("Static prim return: ");
            for (int i = 1; i < prims.Length; i++)
            {
                Console.Write(" " + prims[i]);
            }

            Console.WriteLine("\n");

            // Dynamic list
            List<int> priml = returnList(array);
            Console.Write("List prim return: ");
            priml.ForEach(delegate (int p)
            {
                if (p != 0)
                {
                    Console.Write(" " + p);
                }
            });

            Console.WriteLine("\n");

            // Dynamic dictionary
            Dictionary<int, int> primd = returnDictionary(array);
            Console.Write("Dictionary prim return: ");
            foreach (KeyValuePair<int, int> p in primd)
            {
                Console.Write(" " + p.Value);
            }

            Console.WriteLine("\n");
            Console.ReadKey();
        }

        static PrimeType[] Eratosthenes(int length)
        {
            PrimeType[] array = new PrimeType[length];
            int max = (int)Math.Sqrt(length);

            for (int i = 2; i < max; i++)
            {
                if (array[i] == PrimeType.Prim)
                {
                    for (int j = 1; j < (length / 2 + 1); j++)
                    {
                        if (i * j < length)
                        {
                            array[i * j] = PrimeType.NotPrim;
                        }
                    }
                }
            }

            return array;
        }

        static int[] returnStatic(PrimeType[] array)
        {
            int size = 0;

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] == PrimeType.Prim)
                {
                    size++;
                }
            }

            int[] prim = new int[size];
            int counter = 0;

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] == PrimeType.Prim)
                {
                    prim[counter] = i;
                    counter++;
                }
            }

            return prim;
        }

        static List<int> returnList(PrimeType[] array)
        {
            List<int> prim = new List<int>();

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] == PrimeType.Prim)
                {
                    prim.Add(i);
                }
            }

            return prim;
        }

        static Dictionary<int, int> returnDictionary(PrimeType[] array)
        {
            Dictionary<int, int> prim = new Dictionary<int, int>();
            int counter = 0;

            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] == PrimeType.Prim)
                {
                    prim.Add(counter, i);
                    counter++;
                }
            }

            return prim;
        }

    }
}
