﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab06
{
    public partial class WebBrowser : Form
    {

        [STAThread]
        public static void Main()
        {
            NISTTime.SetTime();
            /*
            Contacts contacts = new Contacts();
            contacts.ReadCsv(@"C:\Users\Patrick\Repository\zhaw-dnet\Lab06\Lab06\ZHAW-Namelist.csv");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WebBrowser());*/

        }

        public WebBrowser()
        {
            InitializeComponent();
            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
            webBrowser1.Navigate("https://mail.zhaw.ch");

        }

        private void webBrowser1_DocumentCompleted(Object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //e.Url.ToString().StartsWith("https://mail.zhaw.ch/OWA/auth/logon.aspx");

            HtmlDocument document = webBrowser1.Document;
            HtmlElement formUser = document.GetElementById("username");
            HtmlElement formPass = document.GetElementById("password");
            if (formUser != null && formPass != null)
            {
                formUser.SetAttribute("value", "<StudentenName>");
                formPass.SetAttribute("value", "******");

                foreach (HtmlElement elem in formPass.GetElementsByTagName("input"))
                {
                    if (elem.GetAttribute("value") == "Anmelden") { elem.InvokeMember("click"); }
                }
            }
        }

    }
}
