
using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Lab06 {

    public class Contacts : List<Contact> {

        public void ReadCsv(string inFile) {
            FileStream fs;
            TextReader tr;
            fs = new FileStream(inFile, FileMode.Open, FileAccess.Read);
            tr = new StreamReader(fs, Encoding.GetEncoding("windows-1252"));
            string line = tr.ReadLine();
            this.Clear();
            while ((line = tr.ReadLine()) != null) {
                string[] splitted = line.Split(';');
                Add(new Contact(
                    splitted[2], 
                    splitted[1],
                    "ZHAW",
                    splitted[6], 
                    splitted[8],
                    splitted[0],
                    (splitted[3] != "") ? splitted[3].ToLower() + "@zhaw.ch" : "",
                    splitted[3], 
                    splitted[4], 
                    splitted[7]));
            }
            tr.Close();
            fs.Close();
        }


        /*Contact(
        string FirstName, 
        string LastName,
        string Company,
        string Department,
        string JobTitle,
        string BusinessPhone,
        string EmailAddress,
        string Initials,
        string OfficeLocation,
        string Profession
        )*/

        private void WriteQP(TextWriter tw, string s, string t) {
            tw.WriteLine(QuotedPrintableEncoding.Encode(s.Length, s + t));
        }

        public void WriteVcf(Contact c) {
            string outFile = (c.FirstName + " " + c.LastName + ".vcf");
            FileStream outStream = new FileStream(outFile.ToLower(), FileMode.Create, FileAccess.Write);
            TextWriter tw = new StreamWriter(outStream, Encoding.GetEncoding("windows-1252"));
            WriteQP(tw, "BEGIN:VCARD", "");
            WriteQP(tw, "VERSION:2.1", "");
            WriteQP(tw, "N;ENCODING=QUOTED-PRINTABLE:", c.LastName + ";" + c.FirstName);
            WriteQP(tw, "FN;ENCODING=QUOTED-PRINTABLE:", c.FirstName + " " + c.LastName);
            WriteQP(tw, "ORG;ENCODING=QUOTED-PRINTABLE:", "ZHW, " + c.Department);
            WriteQP(tw, "TEL;WORK;VOICE;ENCODING=QUOTED-PRINTABLE:", c.BusinessPhone);
            WriteQP(tw, "TEL;HOME;VOICE;ENCODING=QUOTED-PRINTABLE:", c.HomePhone);
            WriteQP(tw, "ROLE;QuotedPrintableEncoding.Encode(s.Length,s):", c.JobTitle);
            WriteQP(tw, "EMAIL;PREF;INTERNET;ENCODING=QUOTED-PRINTABLE:", c.EmailAddress);
            WriteQP(tw, "UID;ENCODING=QUOTED-PRINTABLE:", c.Initials.ToUpper());

            WriteQP(tw, "ADR;HOME;ENCODING=QUOTED-PRINTABLE:", ";;" + c.HomeStreet + ";" + c.HomeCity + ";;" + c.HomePostalCode + ";");
            WriteQP(tw, "END:VCARD", "");
            tw.Close();
            outStream.Close();
        }

        public void WriteVcf() {
            foreach (Contact c in this) {
                WriteVcf(c);
            }
        }

        public void WriteCsv(Locale locale, string outFile) {
            FileStream outStream = new FileStream(outFile, FileMode.Create, FileAccess.Write);
            TextWriter tw = new StreamWriter(outStream, Encoding.GetEncoding("windows-1252"));

            StringBuilder sb = new StringBuilder();
            this.Insert(0, new Contact(locale));
            foreach (Contact c in this) {
                string delim2 = "";
                foreach (string s in c) {
                    sb.Append(delim2 + "\"" + s + "\"");
                    delim2 = ",";
                }
                sb.Append(Environment.NewLine);
                Console.WriteLine(c.ToString());
            }
            tw.Write(sb);
            tw.Close();
            this.RemoveAt(0);
            outStream.Close();
        }

        private const string URI = "https://160.85.192.55/personensuche/okv_vcard2.php?PersonSign=";
        private string user;
        private string pwd;
        private string[] processResponse(Stream receiveStream, Encoding encode) {
            StreamReader sr = new StreamReader(receiveStream, encode);
            string strline;
            string strRegex = "<tr><td>-<br>(?<street>[^<]+)<BR>(?<zip>[^<]+)&nbsp;(?<city>[^<]+)(<BR>-<br>Tel: (?<phone>[^<]+))?";
            string[] homeAddress = null;
            while ((strline = sr.ReadLine()) != null && homeAddress == null) {
                Regex regex = new Regex(strRegex);
                Match m = regex.Match(strline);
                if (m.Success) {
                    homeAddress = new string[] { m.Groups["street"].Value, m.Groups["zip"].Value, m.Groups["city"].Value, m.Groups["phone"].Value };
                    break;
                }
            }
            sr.Close();
            return homeAddress;
        }

        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public void UpdatePrivateContact(Contact c, string User, string Pwd) {
            try {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                String strURI = URI + c.Initials;
                //Console.WriteLine(c.LastName);
                HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create(strURI);
                NetworkCredential networkCredential = new NetworkCredential(User, Pwd);
                myWebRequest.Credentials = networkCredential;
                myWebRequest.AllowAutoRedirect = true;
                WebResponse myWebResponse = myWebRequest.GetResponse();
                Stream receiveStream = myWebResponse.GetResponseStream();
                Encoding encode = Encoding.GetEncoding("windows-1252");
                string[] homeAddress;
                
                homeAddress = processResponse(receiveStream, encode);
                myWebResponse.Close();
                if (homeAddress != null)
                {
                    c.HomeStreet = homeAddress[0];
                    c.HomePostalCode = homeAddress[1];
                    c.HomeCity = homeAddress[2];
                    c.HomePhone = homeAddress[3];
                }
                else
                {
                  //  Telsearch service = new Telsearch();
                }
                Thread t = Thread.CurrentThread;
                Console.WriteLine(t.GetHashCode() + ":" + c.ToString());
            }
            catch (Exception e) {
                Console.WriteLine("ERROR:" + Thread.CurrentThread.GetHashCode() + ":" + c.ToString()+e,ToString());
            }
        }


        private int nextContact;
        private Contact NextContact() {
            lock (this) {
                if (nextContact < this.Count) return (Contact)this[nextContact++];
                return null;
            }
        }

        private void RunUpdatePrivateContacts() {
            Contact c = NextContact();
            while (c != null) {
                if (true ||  c.Profession.StartsWith("Doz")) UpdatePrivateContact(c, user, pwd);
                c = NextContact();
            }

        }

        public void UpdatePrivateContacts(string User, string Pwd) {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
            Console.WriteLine("private updating ...");
            user = User; pwd = Pwd;
            Thread[] t = new Thread[10];
            for (int i = 0; i < t.Length; i++) {
                t[i] = new Thread(new ThreadStart(RunUpdatePrivateContacts));
                t[i].Start();
            }
            for (int i = 0; i < t.Length; i++) {
                t[i].Join();
            }
            Console.WriteLine("private updated");
        }

        public void WriteXml(string fileName) {
            XmlSerializer serializer = new XmlSerializer(this.GetType(),new XmlRootAttribute("Contacts"));
            FileStream fs = new FileStream(fileName, FileMode.Create);
            // Objekt �ber ein StreamWriter-Objekt serialisieren
            StreamWriter streamWriter = new StreamWriter(fs, Encoding.GetEncoding("iso-8859-1"));
            serializer.Serialize(streamWriter, this);
            streamWriter.Close();
            fs.Close();
        }


        /* Methode zum Deserialisieren eines Objekts aus einer XML-Datei */
        public void ReadXml(string fileName) {
            // XmlSerializer f�r den Typ des Objekts erzeugen
            XmlSerializer serializer = new XmlSerializer(this.GetType(), new XmlRootAttribute("Contacts"));
            // Objekt �ber ein StreamReader-Objekt serialisieren
            StreamReader streamReader = new StreamReader(fileName, Encoding.GetEncoding("iso-8859-1"));
            Contacts cA = (Contacts)serializer.Deserialize(streamReader);
            streamReader.Close();
            this.Clear();
            foreach (Contact c in cA) Add(c);
        }
    }
}
