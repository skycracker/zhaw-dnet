using System;
using System.Collections;
using System.Reflection;

namespace Lab06 {
    public enum Locale { EN, DE };
    public class Contact : IComparable {
        public string Title;
        public string FirstName;
        public string LastName;
        public string Company;
        public string Department;
        public string JobTitle;
        public string BusinessStreet;
        public string BusinessCity;
        public string BusinessPostalCode;
        public string BusinessCountry;
        public string HomeStreet;
        public string HomeCity;
        public string HomeState;
        public string HomePostalCode;
        public string HomeCountry;
        public string BusinessFax;
        public string BusinessPhone;
        public string HomePhone;
        public string MobilePhone;
        public string Birthday;
        public string Categories;
        public string EmailAddress;
        public string EmailDisplayName;
        public string Email2Address;
        public string Email2DisplayName;
        public string Gender;
        public string Initials;
        public string Language;
        public string Notes;
        public string OfficeLocation;
        public string POBox;
        public string Priority;
        public string Private;
        public string Profession;
        public string WebPage;
        public string Nickname;

        public Contact(Locale locale) {
            if (locale == Locale.EN) {
                Title = "Title";
                FirstName = "First Name";
                LastName = "Last Name";
                Company = "Company";
                Department = "Department";
                JobTitle = "Job Title";
                BusinessStreet = "Business Street";
                BusinessCity = "Business City";
                BusinessPostalCode = "Business Postal Code";
                BusinessCountry = "Business Country";
                HomeStreet = "Home Street";
                HomeCity = "Home City";
                HomeState = "Home State";
                HomePostalCode = "Home Postal Code";
                HomeCountry = "Home Country";
                BusinessFax = "Business Fax";
                BusinessPhone = "Business Phone";
                HomePhone = "Home Phone";
                MobilePhone = "Mobile Phone";
                Birthday = "Birthday";
                Categories = "Categories";
                EmailAddress = "E-mail Address";
                EmailDisplayName = "E-mail Display Name";
                Email2Address = "E-mail 2 Address";
                Email2DisplayName = "E-mail 2 Display Name";
                Gender = "Gender";
                Initials = "Initials";
                Language = "Language";
                OfficeLocation = "Office Location";
                Notes = "Notes";
                POBox = "PO Box";
                Priority = "Priority";
                Private = "Private";
                Profession = "Profession";
                WebPage = "Web Page";
                Nickname = "Nickname";
            }
            if (locale == Locale.DE) {
                Title = "Anrede";
                FirstName = "Vorname";
                LastName = "Nachname";
                Company = "Firma";
                Department = "Abteilung";
                JobTitle = "Position";
                BusinessStreet = "Straße geschäftlich";
                BusinessCity = "Ort geschäftlich";
                BusinessPostalCode = "Postleitzahl geschäftlich";
                BusinessCountry = "Land geschäftlich";
                HomeStreet = "Straße privat";
                HomeCity = "Ort privat";
                HomeState = "Region privat";
                HomePostalCode = "Postleitzahl privat";
                HomeCountry = "Land privat";
                BusinessFax = "Fax geschäftlich";
                BusinessPhone = "Telefon geschäftlich";
                HomePhone = "Telefon privat";
                MobilePhone = "Mobiltelefon";
                Birthday = "Geburtstag";
                Categories = "Kategorien";
                EmailAddress = "E-Mail-Adresse";
                EmailDisplayName = "E-Mail: Angezeigter Name";
                Email2Address = "E-Mail 2: Adresse";
                Email2DisplayName = "E-Mail 2: Angezeigter Name";
                Gender = "Geschlecht";
                Initials = "Initialen";
                Language = "Sprache";
                Notes = "Notizen";
                OfficeLocation = "Büro";
                POBox = "Postfach geschäftlich";
                Priority = "Priorität";
                Private = "Privat";
                Profession = "Beruf";
                WebPage = "Webseite";
                Nickname = "Stichwort";
            }
        }

        public IEnumerator GetEnumerator() {
            // Get the properties in the object
            FieldInfo[] fields = this.GetType().GetFields();
            // loop through the properties
            foreach (FieldInfo property in fields) {
                yield return property.GetValue(this);
            }

        }

        public Contact(string firstName, string lastName,
                       string company,
                       string department, string jobTitle,
                       string businessPhone,
                       string emailAddress,
                       string initials, string officeLocation, string profession
          ) {
            Title = "";
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Company = company;
            this.Department = department;
            this.JobTitle = jobTitle;
            BusinessStreet = "";
            BusinessCity = "";
            BusinessPostalCode = "";
            BusinessCountry = "";
            HomeStreet = "";
            HomeCity = "";
            HomeState = "";
            HomePostalCode = "";
            HomeCountry = "";
            BusinessFax = "";
            this.BusinessPhone = businessPhone;
            HomePhone = "";
            MobilePhone = "";
            Birthday = "";
            Categories = "";
            this.EmailAddress = emailAddress;
            this.EmailDisplayName = firstName + " " + LastName;
            Email2Address = "";
            Email2DisplayName = "";
            Gender = "";
            this.Initials = initials;
            Language = "";
            Notes = "";
            POBox = "";
            Priority = "";
            Private = "";
            this.OfficeLocation = officeLocation;
            this.Profession = profession;
            WebPage = "";
        }

        public int CompareTo(object obj) {
            Contact c = obj as Contact;
            return (this.LastName + this.FirstName).CompareTo(c.LastName + c.FirstName);
        }

        public Contact() {
        }

        public override string ToString() {
            return FirstName + " " + LastName + " " + JobTitle + ", " + Company + " " + Department + " " + OfficeLocation;
        }
    }
}
