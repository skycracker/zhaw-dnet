﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine(" LAB 2 .NET - Integrator");
            Console.WriteLine(" -----------------------\n");

            Integrator test = new Integrator();
            Console.WriteLine(" The result is: " + test.Integrate(new MyFunction(), 0,10, 0.0001));

            Console.ReadKey();
        }
    }
}
