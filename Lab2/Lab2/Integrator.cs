﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Integrator
    {
        public double Integrate(IFunction function, int a, int b, double accurary)
        {
            double N = 100;
            double diff = Double.PositiveInfinity;

            double n1 = 0;
            double n2 = integrate(function, a, b, N); ;
            

            while (diff > accurary)
            {
                N = N*2;
                n1 = n2;
                n2 = integrate(function, a, b, N);
                diff = n2 - n1;

                Console.WriteLine(" Result not accurate enough : " + n1);

            }
            return n2;
        }


        private double integrate(IFunction function, int a, int b, double N)
        {
            
            double h = (b - a) / N;
            double res = (function.F(a) + function.F(b)) / 2;
            for (int i = 1; i < N; i++)
            {
                res += function.F(a + i * h);
            }
            return h * res;
        }

    }
}
