﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class MyFunction : IFunction
    {
        public double F(double x)
        {
            return 3*x/(x*x +1);
        }
    }
}
