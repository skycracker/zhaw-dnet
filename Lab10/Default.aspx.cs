﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CalculateBMI(object sender, EventArgs e)
    {
        double heightVal = 0;
        int weightVal = 0;

        try
        {
            weightVal = Convert.ToInt32(weight.Text);
            heightVal = Convert.ToDouble(height.Text);
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
        }

        if (weightVal < 1 || heightVal < 1)
        {
            bmi.Text = "Ungültige Grösse oder Höhe.";
        }
        else if (weightVal > 200)
        {
            bmi.Text = "Viel zu schwer ...";
        }
        else if (heightVal > 3)
        {
            bmi.Text = "So gross is niemand ...";
        }
        else
        {
            double bmiVal = weightVal/(heightVal*heightVal);
            if (bmiVal < 0 || bmiVal > 70)
            {
                bmi.Text = "Ungültige Grösse oder Höhe.";
            }
            else
            {
                bmi.Text = bmiVal.ToString();
            }

            
        }

    }

}