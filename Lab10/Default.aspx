﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            margin-top: 100px;
            margin-left: 100px;
        }
         div {
             display: block;
             width: 400px;
             margin-bottom: 2px;
             display: block;
             clear: both;
         }
        input, span {
            float: left;
            display: block;
        }
        span {
            width: 40%;
        }
        input {
            width: 58%;
        }
        input[type=submit] {
            width: auto;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        
        <div>
            <asp:Label ID="weightL" Text="Gewicht in kg:" Runat="server"/>
            <asp:TextBox ID="weight" Runat="server"/>
        </div>
        
        <div>
            <asp:Label ID="heightL" Text="Grösse in m:" Runat="server"/>
            <asp:TextBox ID="height" Runat="server"/>
        </div>
        
        <div>
            <asp:Button ID="reset" Text="reset" Runat="server" />
            <asp:Button ID="submit" Text="berechnen" OnClick="CalculateBMI" Runat="server" />
        </div>
        
        <div>
            <asp:Label ID="bmiL" Text="BMI:" Runat="server"/>
            <asp:TextBox ID="bmi" Runat="server"/>
        </div>

    </form>
</body>
</html>
