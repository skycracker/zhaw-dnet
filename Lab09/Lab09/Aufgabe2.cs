﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab09
{
    public partial class Aufgabe2 : Form
    {

        static string strConnection = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=app97.mdb; Persist Security Info=False;";
        private static string table = "Appointments";

        DataSet dataSet = new DataSet();
        DataView dataView = null;

        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Aufgabe2());
        }

        public Aufgabe2()
        {
            InitializeComponent();
        }

        void evt_LoadMDB(Object sender, EventArgs e)
        {
            LoadTable(dataSet, table);
            dataView = dataSet.Tables["Appointments"].DefaultView;
            dataGridView1.DataSource = dataView;
            button5.Enabled = true;
        }

        void evt_StoreMDB(Object sender, EventArgs e)
        {
            if (dataSet.HasChanges())
            {
                StoreTable(dataSet, table);
            }
            else
            {
                Console.WriteLine("No changes in the dataset.");
            }
        }

        void evt_Filter(Object sender, EventArgs e)
        {
            string date = dateTimePicker1.Value.ToString("MM.dd.yyyy");
            string filter = "Start > #" + date + " 00:00:00# AND Start < #" + date + " 23:59:59#";
            dataView.RowFilter = filter;
        }

        static void LoadTable(DataSet ds, string tableName)
        {
            OleDbConnection con = new OleDbConnection(strConnection);
            //----- SelectCommand setzen
            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + tableName, con);
            //-----füge Schema Information automatisch hinzu
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            adapter.Fill(ds, tableName);
            if (ds.HasErrors)
            {
                ds.RejectChanges();
                throw new Exception("Error Loading Data");
            }
            else ds.AcceptChanges();
            adapter.Dispose();
            Console.WriteLine("Loaded Table:" + tableName);
        }

        static void StoreTable(DataSet ds, string tableName)
        {
            OleDbConnection con = new OleDbConnection(strConnection);
            //----- SelectCommand setzen, damit der OleDbCommandBuilder automatisch
            // Insert-, Update- und Delete-Kommandos generieren kann
            OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM " + tableName, con);
            OleDbCommandBuilder cmdBuilder = new OleDbCommandBuilder(adapter);
            cmdBuilder.QuotePrefix = "["; cmdBuilder.QuoteSuffix = "]";
            //----- Daten speichern!
            adapter.Update(ds, tableName);
            adapter.Dispose();
            Console.WriteLine("Stored Table:" + tableName);
        }

        static void Print(DataSet ds)
        {
            Console.WriteLine("DataSet {0}:", ds.DataSetName);
            Console.WriteLine();
            foreach (DataTable t in ds.Tables)
            {
                Print(t);
                Console.WriteLine();
            }
        }

        static void Print(DataTable t)
        {
            //---- Tabellenkopf
            Console.WriteLine("Tabelle {0}:", t.TableName);
            foreach (DataColumn col in t.Columns)
            {
                Console.Write(col.ColumnName + "|");
            }
            Console.WriteLine();
            for (int i = 0; i < 40; i++) { Console.Write("-"); }
            Console.WriteLine();
            //---- Daten
            int nrOfCols = t.Columns.Count;
            foreach (DataRow row in t.Rows)
            {
                for (int i = 0; i < nrOfCols; i++)
                {
                    Console.Write(row[i]); Console.Write("|");
                }
                Console.WriteLine();
            }
        }
    }
}
