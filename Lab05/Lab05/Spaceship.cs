using System.Drawing;
using System.Collections;
using System.Collections.Generic;

namespace Lab05
{

    class Spaceship : Orb
    {

        public Spaceship(double x, double y, double vx, double vy, double m)
            : base("enterprise", x, y, vx, vy, m)
        {
        }


        public override void Draw(Graphics g)
        {
            double w = bitmap.Width / 2;
            double h = bitmap.Height / 2;
            g.DrawImage(bitmap, (int)(pos[0] - w / 2), (int)(pos[1] - h / 2), (int)w, (int)h);

        }

        public override void CalcPosNew(IList<Orb> space)
        {
            base.CalcPosNew(space);
            foreach (Orb orb in new List<Orb>(space))
            {
                if (orb != this)
                {
                    Vektor distance = orb.Pos - pos;
                    if (distance <= 20)
                    {
                        DoCollision();
                    }
                }
            }
        }
    }	
}
