using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Lab05
{
	class Explosion: Orb
	{
		public Explosion(double x, double y) 
			: base("explosion",x,y,0,0,0) 
		{
		}
		
        private double call = 1;

		public override void Draw(Graphics g)
		{
			int w = (int) (bitmap.Width/2*call);
			int h = (int) (bitmap.Height/2*call);

            call += 0.1;
            
			g.DrawImage(bitmap,(int)pos[0]-w/2,(int)pos[1]-h/2,w,h);

        }

        public override void CalcPosNew(IList<Orb> space)
        {
            posNew = pos;

            if (call > 2)
            {
                space.Remove(this);
            }

        }

    }
}
