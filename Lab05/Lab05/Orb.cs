using System;
using System.Collections.Generic;
using System.Drawing;

namespace Lab05
{

    delegate void CollisionHandler(Orb obj);

    abstract class Orb
	{
	    public event CollisionHandler Collision;

        const double  G = 30; //6.673e-11

        protected Bitmap bitmap;
		protected Vektor posNew,pos;
		protected Vektor v0;
		protected string name;
		protected double masse;

        // Event Activities
	    public void DoCollision()
	    {
	        if (Collision != null)
	        {
	            Collision(this);
	        }
	    }


        public Vektor Pos 
		{
			get {return pos;}
            set { this.pos = value;  }
		}
		
		public Vektor Velocity {
			get {return v0;}
		}
		
		public double Mass
		{ 
			get {return masse;}
		}
		public abstract void Draw(Graphics g);
		
		public void Move() 
		{
			pos = posNew;
		}
		
		public Orb(string name,double x, double y, double vx, double vy, double m)
		{
			bitmap = new Bitmap(name+".gif");
            bitmap.MakeTransparent(bitmap.GetPixel(1, 1));
			pos = new Vektor(x,y,0);
			v0 = new Vektor(vx,vy,0);
			masse = m;
			this.name = name;
		}
		
		public virtual void CalcPosNew(IList<Orb> space)
		{
			
			//f�r alle Objekte im Array o ausser dem eigenen (mit this erkennbar?)
			Vektor a = new Vektor(0,0,0); //f�r Gesamtbeschleunigung
			foreach (Orb o in space) 
			{
				if (o!=this)
				{
					//radius berechnen
					Vektor abstand = o.Pos-pos;
					double r = (double)abstand; 
					//if (o is Spaceship) System.Console.WriteLine(r);
					a += (G*o.Mass/(r*r*r))*abstand;
				}
			}
			//neue Position berechnen
			double t=3;
			posNew = pos + v0*t + (t*t)*a;
			v0 = v0 + t*a;
		}
		
		public override string ToString()
		{
			return name;
		}
		

		
	}
}
