using System.Drawing;

namespace Lab05
{
	class Planet: Orb
	{
		public Planet(string name,double x, double y, double vx, double vy, double m) 
			: base(name,x,y,vx,vy,m) 
		{
		}
		
		public override void Draw(Graphics g)
		{
			int w = bitmap.Width/2;
			int h = bitmap.Height/2;
            
			g.DrawImage(bitmap,(int)pos[0]-w/2,(int)pos[1]-h/2,w,h);
		}
	}
}
