﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab05
{
    [AttributeUsage(AttributeTargets.All)]
    public class CollidableAttribute : Attribute
    {
        private bool isCollidable;

        public CollidableAttribute()
        {
            this.isCollidable = true;
        }

        public CollidableAttribute(bool isCollidable)
        {
            this.isCollidable = isCollidable;
        }
    }
}
