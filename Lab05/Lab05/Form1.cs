using System;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Lab05
{
    public partial class Form1 : Form
    {
        private IList<Orb> space = new List<Orb>();
        private Timer timer1;
        private Spaceship spaceship;
        private Explosion explosion;

        public Form1()
        {
            InitializeComponent();
            this.BackColor = System.Drawing.Color.Aqua;
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);

            //Objekte im Weltall positionieren
            
            spaceship = new Spaceship(600, 230, 3, 0, 1);
            spaceship.Collision += new CollisionHandler(OnCollideAction);
            space.Add(spaceship);
            space.Add(new Planet("jupiter", 600, 400, -0.099, 0, 100));
            space.Add(new Planet("mars", 300, 400, 0, 1.5, 4));
            space.Add(new Planet("merkur", 200, 200, 0, -1.5, 4));
            
            /*
            space.Add(new Planet("jupiter", 600, 400, -0.038, 0, 100));
            space.Add(new Planet("mars", 600, 600, 3.8, 0, 1));
            */

            explosion = new Explosion(0,0);

            //Timer implementieren. 
            //L�st vom benutzerdefinierte Ereignisse in einem bestimmten Intervall aus.
            timer1 = new Timer();
            timer1.Interval = 50;
            timer1.Start();
            timer1.Tick += new EventHandler(timer1_Tick);
        }

        public void timer1_Tick(object sender, EventArgs eArgs)
        {
            foreach (Orb o in new List<Orb>(space)) o.CalcPosNew(space);
            foreach (Orb o in new List<Orb>(space)) o.Move();
            this.Refresh();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            foreach (Orb o in space) o.Draw(e.Graphics);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SuspendLayout();
            // 
            // Form1
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1158, 768);
            this.Name = "Space Simulator 2016";
            this.ResumeLayout(false);

        }

        void OnCollideAction(Orb orb)
        {
            space.Remove(orb);
            explosion.Pos = orb.Pos;
            explosion.CalcPosNew(space);
            space.Add(explosion);
            Console.WriteLine(orb.ToString() + " ist kollidiert.");
        }
    }
}