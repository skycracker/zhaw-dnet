﻿using System;

namespace Lab03
{
    class Vektor
    {

        private double[] coordinates = new double[3];

        public Vektor(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        // Getters and Setters

        public double X
        {
            set { coordinates[0] = value; }
            get { return coordinates[0]; }
        }

        public double Y
        {
            set { coordinates[1] = value; }
            get { return coordinates[1]; }
        }

        public double Z
        {
            set { coordinates[2] = value; }
            get { return coordinates[2]; }
        }

        // Indexer

        public double this[int index]
        {
            get { return coordinates[index]; }
            set { coordinates[index] = value; }
        }

        // Operators

        public static Vektor operator +(Vektor a, Vektor b)
        {
            return new Vektor(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vektor operator -(Vektor a, Vektor b)
        {
            return new Vektor(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vektor operator *(Vektor a, Vektor b)
        {
            return new Vektor(
                a.Y * b.Z - a.Z * b.Y,
                a.Z * b.X - a.X * b.Z,
                a.X * b.Y - a.Y * b.X
            );
        }

        // Conversation

        public static implicit operator Vektor(double x)
        {
            return new Vektor(x,0,0);
        }

        public static implicit operator double(Vektor v)
        {
            return Math.Sqrt(Math.Pow(v.X, 2) + Math.Pow(v.Y, 2) + Math.Pow(v.Z, 2));
        }

        public override string ToString()
        {
            return "[ " + this.X + " " + this.Y + " " + this.Z + " ]";
        }

        public override bool Equals(object obj)
        {
            Vektor v = (Vektor) obj;
            return X.Equals(v.X) && Y.Equals(v.Y) && Z.Equals(v.Z);
        }
    }
}
