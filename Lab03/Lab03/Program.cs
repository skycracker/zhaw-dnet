﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab03
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine(" Lab 03 - Rasend schnell");
            Console.WriteLine(" -----------------------\n");

            Vektor wErde = new Vektor(0, 0, 2 * Math.PI / (24 * 3600));
            Vektor rErde = new Vektor(6370, 0, 0);
            Vektor vErde = wErde * rErde;

            Console.WriteLine("Geschwindigkeit Erdumdrehung: " + vErde);

            Vektor wSonne = new Vektor(0, 0, 2 * Math.PI / (365.25 * 24 * 3600));
            Vektor rSonne = new Vektor(149600000, 0, 0);
            Vektor vSonne = wSonne * rSonne;

            Console.WriteLine("Geschwindigkeit Rotation um Sonne: " + vSonne);

            Vektor wZentrum = new Vektor(0, 0, 2 * Math.PI / (225000000.0 * 365.25 * 24 * 3600));
            Vektor rZentrum = new Vektor(25000 * 9.46 * Math.Pow(10, 12), 0, 0);
            Vektor vZentrum = wZentrum * rZentrum;

            Console.WriteLine("Geschwindigkeit Rotation um Zentrum: " + vZentrum);

            Vektor vTotal = vErde + vSonne + vZentrum;
            Console.WriteLine("Geschwindigkeit Total: " + vTotal.Y + " (Lösung ~239 kms-1)");

            Console.ReadKey();


        }
    }
}
